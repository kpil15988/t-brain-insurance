# The content is divided into four parts.

1. EDA


2. Base XGBoost Model


3. Feature Engineering


4. Final model

一、	資料處理與特徵選取
1.	首先先將沒有順序性的類別變數轉成虛擬變數(Dummy variables)，順序變數(Ordinal variables)維持不變。
2.	因為我們猜測有些變數的遺失值具有特別意義，如Level和RFM_M_LEVEL，因此對這些具有大量NA的變數，做標記型變數，舉例來說，生一個新的欄位，當Level變數若為NA則該欄位標記為1，非NA則標示為0，則這些變數再原本變數名稱前新增new，如：new_level。
3.	考慮所有類別和連續變數之間的交乘項，試圖尋找變數的交乘項中，是否存在一些重要變數。做法如下。

Step1: 生成所有變數的交乘項。

Step2: 將變數隨機每200個分成一等分，做一個XGB模型，每一個等分之間的變數不重複。

Step3: 在每一個XGB模型中選出前6個重要的變數。

Step4: 將所有選出來的變數，將變數總合重新組合，再製造一個新的模型，再重新選出前30個。

Step5: 將原始的資料中存在的變數與這10個變數組合，去除重複的變數後，再重新配適最終預測模型。

4.	模型中的變數除了原始檔案中的131個變數外(類別變數轉為dummy variables)，新增以下變數，大部分為重要之交乘項變數：

a.	ANNUAL_PREMIUM_AMT : CHANNEL_A_POL_CNT

b.	L1YR_C_CNT : AGE

c.	LEVEL : LAST_A_ISSUE_DT

d.	new_AGE

e.	new_ RFM_M_level : AGE

f.	APC_1 ST_YEARDIF : INSD_1 ST_AGE

g.	TOOL_VISIT_1YEAR_CNT : DIEBENEFIT_AMT

h.	CHANNEL_A_POL_CNT : L1YR_GROSS_PRE_AMT

i.	INSD_LAST_YEARDIF_CNT : RFM_R
